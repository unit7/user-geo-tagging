import Dependencies._
import sbt.Keys.libraryDependencies

lazy val root = (project in file(".")).
  configs(IntegrationTest).
  settings(
    inThisBuild(List(
      organization := "com.breezzo",
      scalaVersion := "2.11.8",
      version      := "0.1.0"
    )),
    Defaults.itSettings,
    name := "user-geo-tagging",
    libraryDependencies ++= Seq(
      scalaTest % "it,test",
      scalatraTest % "it,test",
      servletApi,
      jettyWebapp % "container,compile",
      scalatra,
      scalatraJson,
      json4s,
      json4sExt,
      scalikejdbc,
      scalikejdbcConfig,
      postgresql,
      h2,
      commonsDbcp,
      logback % Runtime
    )
).enablePlugins(ScalatraPlugin)

mainClass in Compile := Some("com.breezzo.geo.AppLauncher")