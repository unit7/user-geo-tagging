#!/usr/bin/python

import sys
import random

args = sys.argv

records_count = int(args[1])
step = float(args[2])
should_generate_user = len(args) > 3 and '--user_id' == args[3]

i = 1
cur_x = -180
f = open('data.txt', 'w')

while (cur_x < 180 and i <= records_count):
    cur_y = -180
    while (cur_y < 180 and i <= records_count):
        xy = "{0:.2f};{1:.2f}".format(cur_x, cur_y)
        if (should_generate_user):
            f.write(str(i) + ";" + xy + "\r\n")
        else:
            f.write(xy + ";" + "{0:.2f}".format(random.uniform(0, step) * 10) + "\r\n")

        i += 1
        cur_y += step

    cur_x += step

f.close()