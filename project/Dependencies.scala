import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.4"
  lazy val scalatraTest = "org.scalatra" %% "scalatra-scalatest" % "2.5.1"
  lazy val scalatra = "org.scalatra" %% "scalatra" % "2.5.1"
  lazy val scalikejdbc = "org.scalikejdbc" %% "scalikejdbc" % "3.1.0"
  lazy val scalikejdbcConfig = "org.scalikejdbc" %% "scalikejdbc-config" % "3.1.0"
  lazy val postgresql = "postgresql" % "postgresql" % "9.1-901-1.jdbc4"
  lazy val jettyWebapp = "org.eclipse.jetty" % "jetty-webapp" % "9.4.7.v20170914"
  lazy val servletApi = "javax.servlet" % "javax.servlet-api" % "4.0.0"
  lazy val scalatraJson = "org.scalatra" %% "scalatra-json" % "2.5.1"
  lazy val json4s = "org.json4s" %% "json4s-jackson" % "3.5.3"
  lazy val json4sExt = "org.json4s" %% "json4s-ext" % "3.5.3"
  lazy val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"
  lazy val h2 = "com.h2database" % "h2" % "1.4.196"
  lazy val commonsDbcp = "org.apache.commons" % "commons-dbcp2" % "2.1.1"
}
