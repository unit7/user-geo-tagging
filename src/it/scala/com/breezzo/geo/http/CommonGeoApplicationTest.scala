package com.breezzo.geo.http

import com.breezzo.geo.db.{DatabaseAccess, TestingMode}
import com.breezzo.geo.model._
import org.json4s.Extraction.decompose
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.scalatest.FunSuiteLike
import org.scalatra.test.scalatest.ScalatraSuite

/**
  * Simple integration test. Checks that all components works fine.
  *
  * @author breezzo
  * @since 10/8/17.
  */
class CommonGeoApplicationTest extends ScalatraSuite with FunSuiteLike {

  implicit val jsonFormats: Formats = JsonFormats.jsonFormats

  addServlet(classOf[GeoRouter], "/geo/*")
  TestingMode.init()

  test("common pipeline") {

    var user = UserGeoTag(1, GeoTag(42.5, 41.2))

    put("/geo/user", json(user), headers()) {
      status should equal(200)
    }

    user = UserGeoTag(user.userId, GeoTag(50, 100))

    post("/geo/user", json(user), headers()) {
      status should equal(200)
    }

    user = UserGeoTag(user.userId, GeoTag(1000, 1000))

    post("/geo/user/location_status", json(user), headers()) {
      status should equal(200)
      parse(response.body).extract[UserLocationStatus] should equal(UserLocationStatus(LocationInfo.FarToTag.name))
    }

    post("/geo/stats", json(GeoTag(1000, 1000)), headers()) {
      status should equal(200)
      parse(response.body).extract[Stats] should equal(Stats(0))
    }

    delete(s"/geo/user/${user.userId}") {
      status should equal(200)
    }
  }

  private def json[T](o: T): String = {
    compact(decompose(o))
  }

  private def headers(): Iterable[(String, String)] = {
    Seq(("Content-Type", "application/json"))
  }
}
