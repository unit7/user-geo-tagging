CREATE TABLE user_geo_tag(
  user_id INTEGER NOT NULL PRIMARY KEY,
  lon NUMERIC NOT NULL,
  lat NUMERIC NOT NULL
);

CREATE TABLE geo_grid(
  tile_x INTEGER NOT NULL,
  tile_y INTEGER NOT NULL,
  distance_error NUMERIC NOT NULL DEFAULT 0,
  PRIMARY KEY(tile_x, tile_y)
);

