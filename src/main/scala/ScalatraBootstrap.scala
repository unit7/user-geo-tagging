import javax.servlet.ServletContext

import com.breezzo.geo.http.GeoRouter
import org.scalatra.LifeCycle

/**
  * @author breezzo
  * @since 10/8/17.
  */
class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext): Unit = {
    context.mount(new GeoRouter(), "/geo")
  }
}
