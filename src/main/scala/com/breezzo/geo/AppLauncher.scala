package com.breezzo.geo

import com.breezzo.geo.db.{DatabaseAccess, TestingMode}
import com.typesafe.config.{Config, ConfigFactory}
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.DefaultServlet
import org.eclipse.jetty.webapp.WebAppContext
import org.scalatra.servlet.ScalatraListener

/**
  * @author breezzo
  * @since 10/9/17.
  */
object AppLauncher {
  def main(args: Array[String]) {

    var environment = ""

    if (args.length == 4) {
      var userTablePath = ""
      var gridTablePath = ""
      args.sliding(2, 2).toList.collect {
        case Array("--user_table", userTable: String) => userTablePath = userTable
        case Array("--grid_table", gridTable: String) => gridTablePath = gridTable
      }

      if (userTablePath.isEmpty || gridTablePath.isEmpty) {
        printUsageAndExit()
      }

      TestingMode.init(userTablePath, gridTablePath)
    } else {
      DatabaseAccess.init("stable")
    }

    val config = ConfigFactory.load()

    val server = new Server(config.getInt("app.port"))
    val context = new WebAppContext()
    context setContextPath "/"
    context.setResourceBase("src/main/webapp")
    context.addEventListener(new ScalatraListener)
    context.addServlet(classOf[DefaultServlet], "/")

    server.setHandler(context)

    server.start
    server.join
  }

  def printUsageAndExit(): Unit = {
    // TODO
    System.exit(-1)
  }
}
