package com.breezzo.geo.dao

import com.breezzo.geo.db.DatabaseAccess
import com.breezzo.geo.model.{GeoCell, GeoTag, UserGeoTag}
import scalikejdbc._

/**
  * @author breezzo
  * @since 10/8/17.
  */
trait UserGeoDAO {

  def createTag(userTag: UserGeoTag): Unit

  def updateTag(userTag: UserGeoTag): Unit

  def findTag(userId: Int): Option[UserGeoTag]

  def deleteTag(userId: Int): Boolean

  def createGeoCell(geoCell: GeoCell)

  def findGeoCell(tileX: Int, tileY: Int): Option[GeoCell]

  def getUsersCountInCell(geoCell: GeoCell): Int
}

object UserGeoDAO extends UserGeoDAO with DatabaseAccess {
  override def createTag(userTag: UserGeoTag): Unit = {
    transactional { implicit session =>
      sql"INSERT INTO user_geo_tag(user_id, lon, lat) VALUES (${userTag.userId}, ${userTag.tag.longitude}, ${userTag.tag.latitude})".update().apply()
    }
  }

  override def updateTag(userTag: UserGeoTag): Unit = {
    transactional { implicit session =>
      sql"UPDATE user_geo_tag SET lon = ${userTag.tag.longitude}, lat = ${userTag.tag.latitude} WHERE user_id = ${userTag.userId}".update().apply()
    }
  }

  override def findTag(userId: Int): Option[UserGeoTag] = {
    readOnly { implicit  session =>
      sql"SELECT lon, lat FROM user_geo_tag WHERE user_id = ${userId}"
        .map(rs =>
          UserGeoTag(
            userId,
            GeoTag(rs.double("lon"), rs.double("lat")))
        )
        .single()
        .apply()
    }
  }

  override def deleteTag(userId: Int): Boolean = {
    transactional { implicit session =>
      sql"DELETE FROM user_geo_tag WHERE user_id = ${userId}".update().apply() > 0
    }
  }

  override def createGeoCell(geoCell: GeoCell): Unit = {
    transactional { implicit session =>
      sql"INSERT INTO geo_grid(tile_x, tile_y, distance_error) VALUES (${geoCell.tileX}, ${geoCell.tileY}, ${geoCell.distanceError})".update().apply()
    }
  }

  override def findGeoCell(tileX: Int, tileY: Int): Option[GeoCell] = {
    readOnly { implicit session =>
      sql"SELECT distance_error FROM geo_grid WHERE tile_x = ${tileX} AND tile_y = ${tileY}"
        .map(rs => GeoCell(tileX, tileY, rs.double("distance_error")))
        .single()
        .apply()
    }
  }

  override def getUsersCountInCell(cell: GeoCell): Int = {
    readOnly { implicit session =>
      sql"SELECT count(1) AS users_count FROM user_geo_tag WHERE cast(lon as INTEGER) = ${cell.tileX} AND cast(lat as INTEGER) = ${cell.tileY}"
        .map(rs => rs.int("users_count"))
        .single()
        .apply()
        .get
    }
  }
}