package com.breezzo.geo.db

import java.sql.Connection

import scalikejdbc.config.DBsWithEnv
import scalikejdbc.{ConnectionPool, DB, DBSession, SQL}

/**
  * @author breezzo
  * @since 10/9/17.
  */
trait DatabaseAccess {
  protected def getConnection: Connection = ConnectionPool.borrow(DatabaseAccess.DB_NAME)
  protected def getDb: DB = DB(getConnection)

  def transactional[T](execution: DBSession => T): T = {
    DB.using(getDb) { db =>
      db.localTx(execution)
    }
  }

  def readOnly[T](execution: DBSession => T): T = {
    DB.using(getDb) { db =>
      db.readOnly(execution)
    }
  }
}

object DatabaseAccess {
  val DB_NAME = 'geo_db

  def init(environment: String): Unit = {
    DBsWithEnv(environment).setup(DB_NAME)
  }
}
