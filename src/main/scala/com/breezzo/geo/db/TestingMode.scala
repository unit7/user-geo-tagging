package com.breezzo.geo.db

import com.breezzo.geo.dao.UserGeoDAO
import com.breezzo.geo.db.DatabaseAccess.DB_NAME
import com.breezzo.geo.model.{GeoCell, GeoTag, UserGeoTag}
import scalikejdbc.config.DBsWithEnv
import scalikejdbc.{ConnectionPool, DB, SQL}

/**
  * @author breezzo
  * @since 10/10/17.
  */
object TestingMode {
  private val DELIMITER: String = ";"

  def init(): Unit = {
    DBsWithEnv("testing").setup(DB_NAME)
    createSchema()
  }

  def init(userTablePath: String, gridTablePath: String): Unit = {
    init()

    io.Source.fromFile(userTablePath).getLines()
      .map(s => s.split(DELIMITER))
      .map(vals => UserGeoTag(vals(0).toInt, GeoTag(vals(1).toDouble, vals(2).toDouble)))
      .foreach(userGeoTag => {
        UserGeoDAO.createTag(userGeoTag)
      })

    io.Source.fromFile(gridTablePath).getLines()
      .map(s => s.split(DELIMITER))
      .map(vals => GeoCell(vals(0).toDouble.toInt, vals(1).toDouble.toInt, vals(2).toDouble))
      .foreach(cell => {
        UserGeoDAO.createGeoCell(cell)
      })
  }

  private def createSchema(): Unit = {
    val ddl = io.Source.fromInputStream(getClass.getResourceAsStream("/database_ddl.sql")).mkString
    DB.using(DB(ConnectionPool.borrow(DB_NAME))) { db =>
      db.localTx { implicit session =>
        SQL(ddl).execute().apply()
      }
    }
  }
}
