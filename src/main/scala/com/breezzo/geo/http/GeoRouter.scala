package com.breezzo.geo.http

import com.breezzo.geo.model.{GeoTag, UserGeoTag, UserLocationStatus}
import com.breezzo.geo.service.UserGeoService
import org.json4s.Formats
import org.scalatra.ScalatraServlet
import org.scalatra.json._

/**
  * @author breezzo
  * @since 10/8/17.
  */
class GeoRouter extends ScalatraServlet with JacksonJsonSupport {

  protected implicit val jsonFormats: Formats = JsonFormats.jsonFormats

  private val userGeoService: UserGeoService = UserGeoService

  before() {
    contentType = formats("json")
  }

  put("/user") {
    val tag = parsedBody.extract[UserGeoTag]
    // TODO error handling
    userGeoService.createTag(tag)
  }

  post("/user") {
    val tag = parsedBody.extract[UserGeoTag]
    if (userGeoService.findTag(tag.userId).isEmpty) {
      // TODO user not found
    }

    userGeoService.updateTag(tag)
  }

  delete("/user/:userId") {
    val userId: Int = params("userId").toInt
    if (!userGeoService.deleteTag(userId)) {
      // TODO user not found
    }
  }

  post("/user/location_status") {
    val tag = parsedBody.extract[UserGeoTag]
    if (userGeoService.findTag(tag.userId).isEmpty) {
      // TODO user not found
    }

    UserLocationStatus(userGeoService.getLocationStatus(tag).name)
  }

  post("/stats") {
    val geoTag = parsedBody.extract[GeoTag]
    userGeoService.getUsersCount(geoTag)
  }
}