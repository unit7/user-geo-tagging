package com.breezzo.geo.http

import org.json4s.{DefaultFormats, Formats}

/**
  * @author breezzo
  * @since 10/10/17.
  */
object JsonFormats {
  lazy val jsonFormats: Formats = DefaultFormats
}