package com.breezzo.geo.model

/**
  * @author breezzo
  * @since 10/8/17.
  */
case class UserGeoTag(userId: Int, tag: GeoTag)
case class GeoTag(longitude: Double, latitude: Double)
case class UserLocationStatus(locationStatus: String)
case class Stats(users_count: Int)
case class GeoCell(tileX: Int, tileY: Int, distanceError: Double)

object LocationInfo {
  sealed abstract class Status(val name: String) {
  }

  case object FarToTag extends Status("far")
  case object CloseToTag extends Status("close")
}