package com.breezzo.geo.service

import com.breezzo.geo.dao.UserGeoDAO
import com.breezzo.geo.model.{GeoTag, LocationInfo, Stats, UserGeoTag}

/**
  * @author breezzo
  * @since 10/8/17.
  */
trait UserGeoService {
  def createTag(userTag: UserGeoTag): Unit
  def updateTag(userTag: UserGeoTag): Unit
  def findTag(userId: Int): Option[UserGeoTag]
  def deleteTag(userId: Int): Boolean
  def getLocationStatus(userGeoTag: UserGeoTag): LocationInfo.Status
  def getUsersCount(geoTag: GeoTag): Stats
}

object UserGeoService extends UserGeoService {
  var userGeoDAO: UserGeoDAO = UserGeoDAO

  override def createTag(userTag: UserGeoTag): Unit = {
    userGeoDAO.createTag(userTag)
  }

  override def updateTag(userTag: UserGeoTag): Unit = {
    userGeoDAO.updateTag(userTag)
  }

  override def findTag(userId: Int): Option[UserGeoTag] = {
    userGeoDAO.findTag(userId)
  }

  override def deleteTag(userId: Int): Boolean = {
    userGeoDAO.deleteTag(userId)
  }

  override def getLocationStatus(userGeoTag: UserGeoTag): LocationInfo.Status = {
    val (tileX, tileY) = toGeoCell(userGeoTag.tag)
    val cell = userGeoDAO.findGeoCell(tileX, tileY)
    var distanceError: Double = 0

    if (cell.isDefined) {
      distanceError = cell.get.distanceError
    }

    val savedTag = findTag(userGeoTag.userId)
    if (savedTag.isEmpty) {
      // TODO error user not found
    }

    val dist = distance(userGeoTag.tag, savedTag.get.tag)

    if (math.abs(dist) > distanceError) LocationInfo.FarToTag else LocationInfo.CloseToTag
  }

  /**
    * @return distance between two coordinates in meters
    */
  private def distance(pa: GeoTag, pb: GeoTag): Double = {
    val deltaLat = math.toRadians(pb.latitude - pa.latitude)
    val deltaLong = math.toRadians(pb.longitude - pa.latitude)
    val a = math.pow(math.sin(deltaLat / 2), 2) +
      math.cos(math.toRadians(pa.latitude)) * math.cos(math.toRadians(pb.latitude)) * math.pow(math.sin(deltaLong / 2), 2)
    val greatCircleDistance = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    3958.761 * greatCircleDistance
  }

  override def getUsersCount(geoTag: GeoTag): Stats = {
    val (tileX, tileY) = toGeoCell(geoTag)
    val cell = userGeoDAO.findGeoCell(tileX, tileY)

    if (cell.isEmpty) {
      return Stats(0)
    }

    Stats(userGeoDAO.getUsersCountInCell(cell.get))
  }

  private def toGeoCell(geoTag: GeoTag): (Int, Int) = {
    (geoTag.longitude.toInt, geoTag.latitude.toInt)
  }
}
